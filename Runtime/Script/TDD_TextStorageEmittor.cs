﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDD_TextStorageEmittor : MonoBehaviour
{
    public TextStorageEmittor m_linked;
    public bool listenToEnterToFlush;
    public void Update()
    {
        if (listenToEnterToFlush && Input.GetKeyDown(KeyCode.Return))
        {
            if (Input.GetKey(KeyCode.LeftShift))
                m_linked.DequeueCharacterStart(2);
            else
                m_linked.DequeueCharacterEnd(2);
        }
    }

    private void Reset()
    {
        m_linked = GetComponent<TextStorageEmittor>();
    }
}
