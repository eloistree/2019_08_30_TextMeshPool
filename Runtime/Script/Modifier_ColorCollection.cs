﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColorCollections", menuName = "Text Mesh Pool/ Color Collection", order = 1)]
public class ColorCollections : ScriptableObject
{

    public int m_index;
    public ColorTagCollection m_colorCollectionSelected = new ColorTagCollection() {} ;
    public ColorTagCollection[] m_colorCollections = new ColorTagCollection[] { new ColorTagCollection("Default") };


    public void Select(string name)
    {
        for (int i = 0; i < m_colorCollections.Length; i++)
        {
            if(m_colorCollections[i].m_collectionName == name)
            {
                m_index = i;
                m_colorCollectionSelected = m_colorCollections[i];
                break;
            }
        }
    }
    public void Select(int index)
    {
        if (m_colorCollections.Length <= 0)
            throw new System.Exception("The collection can't be null.");
        if (index >= m_colorCollections.Length - 1)
            index = m_colorCollections.Length - 1;
        if (index < 0)
            index = 0;
        
        m_colorCollectionSelected = m_colorCollections[index];
    }

    public void Next(bool loop=true)
    {
        ++m_index;
        if (loop)
        {
            if (m_index > m_colorCollections.Length - 1)
                m_index = 0;
        }
        Select(m_index);
    }
    public void Previous(bool loop=true)
    {
        --m_index;
        if (loop)
        {
            if (m_index < 0)
                m_index = m_colorCollections.Length-1;
        }
        Select(m_index);
    }
}


[System.Serializable]
public class ColorTagCollection {

    public string m_collectionName= "Default";
    public List<TaggedColor> m_colors = new List<TaggedColor>();
   
    public ColorTagCollection(string name = "Default")
    {
        this.m_collectionName = name;
    }

    internal TaggedColor GetRandomColorTagged()
    {
        if (m_colors.Count <= 0) return null;
        return m_colors[UnityEngine.Random.Range(0, m_colors.Count)];
    }
}
[System.Serializable]
public class TaggedColor {

    public string m_label= "Default";
    public Color m_color = Color.white;
}
