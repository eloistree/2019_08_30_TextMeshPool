﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColorCollection", menuName = "Text Mesh Pool/ Color Collection", order = 1)]
public class ColorCollectionScriptable : ScriptableObject
{
    public ColorTagCollection m_colorCollection;

    internal TaggedColor GetRandomColorTagged()
    {
        return m_colorCollection.GetRandomColorTagged();
    }
}
