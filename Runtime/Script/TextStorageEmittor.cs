﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TextStorageEmittor : MonoBehaviour
{
    public TextEmmitted m_textEmitted;
    [SerializeField] string m_textStored;

    public bool m_useLowerCase=false;

    
    public void PushDuplicate(bool toLower) {

        m_textEmitted.Invoke(toLower ? m_textStored.ToLower(): m_textStored);
    }

    public void Add(char character)
    {
        m_textStored += character;

    }
    public void Add(string text)
    {
        m_textStored += text;

    }

    public void DequeueCharacterStart(int numberOfCharacter = 1)
    {
        while (numberOfCharacter > 0 && m_textStored.Length > 0)
        {

            char c = m_textStored[0];
            m_textStored = m_textStored.Substring(1);
            string t = "" + c;
            if (m_useLowerCase)
                t = t.ToLower();
            m_textEmitted.Invoke(t);
            numberOfCharacter--;
        }

    }


    public void DequeueCharacterEnd(int numberOfCharacter = 1)
    {
        while (numberOfCharacter > 0 && m_textStored.Length > 0)
        {

            char c = m_textStored[m_textStored.Length-1];
            m_textStored = m_textStored.Substring(0, m_textStored.Length-1);
            string t = "" + c;
            if (m_useLowerCase)
                t = t.ToLower();
            m_textEmitted.Invoke(t);
            numberOfCharacter--;
        }

    }

    public void DequeueAll()
    {
        m_textEmitted.Invoke(m_textStored);
        m_textStored = "";
    }
}

[System.Serializable]
public class TextEmmitted : UnityEvent<string>{ }
