﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimerEvent : MonoBehaviour
{
    public UnityEvent m_tick;
    public float m_timeBetweenTick=2;
    public float m_timeLeft;
  
    void Update()
    {
        m_timeLeft -= Time.deltaTime;
        if (m_timeLeft < 0) {
            m_timeLeft = m_timeBetweenTick;
            m_tick.Invoke();
        }
    }
}
