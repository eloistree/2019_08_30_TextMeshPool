﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class TextMeshBuilder : MonoBehaviour
{
    [SerializeField] TextMeshLinkScriptable m_listOfTextToMeshLink;
    [SerializeField] Dictionary<string, TextToMeshLink> m_registered = new Dictionary<string, TextToMeshLink>();

    [Header("Do on change")]
    public bool m_addPrefab;
    public List<GameObject> m_toAddPrefabs= new List<GameObject>();

    public void Awake()
    {
        m_registered = m_listOfTextToMeshLink.GetDictionary();
    }

    public GameObject GetPrefab(string text)
    {
        if (!m_registered.ContainsKey(text))
            return null;
        return m_registered[text].m_prefrab;
    }
    public GameObject GetInstance(string text)
    {
        GameObject pref = GetPrefab(text);
        if (pref != null)
            return GameObject.Instantiate(pref);
        return null;
    }

    private void OnValidate()
    {
        if (m_toAddPrefabs.Count>0 && m_listOfTextToMeshLink!=null && m_addPrefab) {
            for (int i = 0; i < m_toAddPrefabs.Count; i++)
            {
             m_listOfTextToMeshLink.GetRegistered().Add(
                 new TextToMeshLink() { m_text = m_toAddPrefabs[i].name, m_prefrab = m_toAddPrefabs[i].gameObject });

            }

#if UNITY_EDITOR
            EditorUtility.SetDirty(m_listOfTextToMeshLink);
#endif
            m_toAddPrefabs.Clear();
            m_addPrefab = false;
        }
    }

}

