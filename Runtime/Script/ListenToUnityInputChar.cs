﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ListenToUnityInputChar : MonoBehaviour
{

    public InputCharEvent m_inputDetected;
    public bool m_setAllToLower;
    void Update()
    {
        string text = Input.inputString;
        if (m_setAllToLower)
            text = text.ToLower();
        for (int i = 0; i < text.Length; i++)
        {
            m_inputDetected.Invoke(""+text[i]);
        }
        
    }

    [System.Serializable]
    public class InputCharEvent : UnityEvent<string> {
    }
}
