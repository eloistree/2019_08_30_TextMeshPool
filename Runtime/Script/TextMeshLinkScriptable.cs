﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TextMeshLink", menuName = "Text Mesh Pool/ Text to mesh Links", order = 1)]
public class TextMeshLinkScriptable : ScriptableObject
{

    public List<TextToMeshLink> m_linksOfTextToMesh = new List<TextToMeshLink>();
    public List<TextToMeshLink> GetRegistered() { return m_linksOfTextToMesh; }

    public Dictionary<string, TextToMeshLink> GetDictionary() {
        Dictionary<string, TextToMeshLink> dico = new Dictionary<string, TextToMeshLink>();
        List<TextToMeshLink> links = GetRegistered();
        for (int i = 0; i < links.Count; i++)
        {
            dico.Add(links[i].m_text, links[i]);
        }
        return dico;
    }
}

[System.Serializable]
public class TextToMeshLink
{

    public string m_text;
    public GameObject m_prefrab;
}
