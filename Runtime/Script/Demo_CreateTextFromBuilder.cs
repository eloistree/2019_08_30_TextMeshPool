﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_CreateTextFromBuilder : MonoBehaviour
{

    public Transform m_whereToCreated;
    public TextMeshBuilder m_builder;
    public string m_randomCharToType="abcd()[]";

    public ColorCollectionScriptable m_colorCollection;

    public int m_created;
    public Text m_debugCount;

    public bool m_useRandomColor;
    public bool m_useVertexColoring;
    [Header("Debug")]
    public MaterialCollection materialAvailable;

    public void CreateRandomLetterAtPosition()
    {
        CreateLetterAtPosition(GetRandomChar());



    }

    public void CreateLetterAtPosition(string text) {
        GameObject created = m_builder.GetInstance(text);

        if (created != null)
        {
            m_created++;
            if (m_debugCount)
                m_debugCount.text = "" + m_created;
            created.transform.position = m_whereToCreated.position;
            created.transform.rotation = m_whereToCreated.rotation;
            created.transform.localScale = m_whereToCreated.lossyScale;

            if (m_useRandomColor)
            {
                TaggedColor selectedColor = m_colorCollection.GetRandomColorTagged();
                TaggedMaterial materialUsed;
                if (!materialAvailable.IsContainingMaterial(selectedColor.m_label, out materialUsed))
                    materialUsed = materialAvailable.AddDefaultMaterial(selectedColor.m_label, selectedColor.m_color);
                materialUsed.SetColor(selectedColor.m_color);
                created.GetComponent<Renderer>().sharedMaterial = materialUsed.m_material;
            }
            if (m_useVertexColoring)
            {
                ChangeVertexColorOfMesh(created);
            }


        }
    }

    private static void ChangeVertexColorOfMesh(GameObject created)
    {
        Color32 newColor =
    new Color32(
        (byte)UnityEngine.Random.Range(0, 256),
        (byte)UnityEngine.Random.Range(0, 256),
        (byte)UnityEngine.Random.Range(0, 256),
        255);

        // Get the mesh filter
        Mesh mesh = created.GetComponent<MeshFilter>().sharedMesh;

        // Create an array of colors matching the same length as the mesh's color array
        Color32[] newColors = new Color32[mesh.vertices.Length];
        for (int vertexIndex = 0; vertexIndex < newColors.Length; vertexIndex++)
        {
            newColors[vertexIndex] = newColor;
        }

        // Apply the color
        mesh.colors32 = newColors;
    }

    private Color GetRandomColor()
    {
        return new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);
    }

    private string GetRandomChar()
    {
        return ""+m_randomCharToType[UnityEngine.Random.Range(0, m_randomCharToType.Length)];
    }
}
