﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour
{
    public Vector3 m_euleur;
    public float m_speed=1f;
    public Space m_spaceType = Space.Self;
  
    void Update()
    {
        transform.Rotate(m_euleur* m_speed * Time.deltaTime, m_spaceType);
    }

    private void Reset()
    {
        m_euleur = new Vector3(GetRandom(-180, 180), GetRandom(-180, 180) , GetRandom(-180, 180));
    }

    private float GetRandom(float min, float max)
    {
        return UnityEngine.Random.Range(min, max);
    }
}
