﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class MaterialCollection
{
    public Material m_defaultMaterial;
    public List<TaggedMaterial> m_materials = new List<TaggedMaterial>();

    public TaggedMaterial AddDefaultMaterial(string name, Color color)
    {

       return AddMaterialType(name, m_defaultMaterial, color);
    }
    public TaggedMaterial AddMaterialType(string name, Material material, Color color)
    {
        TaggedMaterial selectedOrCreated=null;
        if (IsContainingMaterial(name, out selectedOrCreated))
        {
            selectedOrCreated.SetColor(color);

        }
        else {
            selectedOrCreated = new TaggedMaterial(name, new Material( material), color);
            m_materials.Add(selectedOrCreated);
        }
        return selectedOrCreated;
    }
    public bool IsContainingMaterial(string name)
    {
        TaggedMaterial tmp;
        return IsContainingMaterial(name, out tmp);
    }
    public bool IsContainingMaterial(string name, out TaggedMaterial material) {
        material = GetMaterial(name);
        return material != null;
    }

    public TaggedMaterial GetMaterial(string name) {
        for (int i = 0; i < m_materials.Count; i++)
        {
            if (m_materials[i].m_name == name)
            {
                return m_materials[i];
            }
        }
        return null;
    }

}

[System.Serializable]
public class TaggedMaterial {
    public string m_name;
    public Material m_material;

    public TaggedMaterial(): this("Default", new Material(Shader.Find("Transparent/Diffuse")),  Color.white) {

    }
    public TaggedMaterial(string name, Material material, Color color)
    {
        this.m_name = name;
        this.m_material = material;
        SetColor(color);
    }

    internal void SetColor(Color color)
    {
        m_material.color = color;
        //m_material.SetColor("_MainTex",color);
    }
}